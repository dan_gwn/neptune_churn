import pandas as pd
import pickle
import os
import math

from sklearn.ensemble import RandomForestClassifier


__all__ = ['save_churned_accounts',
           'get_all_active_accounts',
           'get_all_data',
           'account_churn_prob',
           'save_likely_churns']


MODEL_NAME = "gump3.p"
FEATURES = ["deposit","withdrawal",
            "cum_balance",
            "prev_gdp",
            "unemployment",
            "med_income",
            "ave_amount"]
pth = os.path.realpath(__file__)[:-16]


model = pickle.load(open(pth + MODEL_NAME, "rb"))
model.verbose = 0
data = pd.read_csv(pth + "full_trans.csv")
data.drop(columns = ["churn_1"], inplace = True)

account_data = data[data["date"] == "2020-05-31"]
account_list = list(account_data["account_id"])


def save_churned_accounts(FILEPATH = "", cutoff = None):
    '''
    Saves a csv file of all customers who the model predicts will churn, given a prediction threshold
    Inputs:
        FILEPATH: The file path to where the csv will be saved
        cutoff  : The prediction cutoff
    Outputs:
        None
    '''
    y_hat = model.predict_proba(account_data[FEATURES])
    y_hat_churn = y_hat[:,1]
    final_df = pd.DataFrame()
    final_df["account_id"] = account_data["account_id"]
    final_df["date"] = account_data["date"]
    final_df["prob_churn"] = y_hat_churn
    
    if cutoff is not None:
        final_df = final_df[final_df["prob_churn"] > cutoff]
    
    fp = FILEPATH.join("account_churn.csv")
    final_df.to_csv(fp, index = False)

def get_all_active_accounts():
    '''
    Returns the May data for all active accounts as a pandas dataframe
    Inputs:
        None
    Outputs:
        A pandas dataframe of the active accounts
    '''
    return account_data[["account_id","customer_id","amount"] + FEATURES]

def get_all_data():
    '''
    Returns the whole aggregated monthly data
    Inputs:
        None
    Outputs:
        A pandas dataframe of the data
    '''
    return data

def account_churn_prob(acc_id : int):
    '''
    Returns the the probability that a given account will churn in the next month
    Inputs:
        acc_id : The account ID
    Outputs:
        The probaility that the account will churn    
    '''
    if acc_id not in account_list:
        raise Exception("Not an Active Account!")
    y_hat = model.predict_proba(account_data[account_data["account_id"] == acc_id][FEATURES])
    return y_hat[0][1]

def sort_by_prob():
    '''
    Creates a dataframe of accounts and churn probabilities, sorted from high to low
    Inputs:
        None
    Outputs:
        A sorted pandas dataframe of accounts and churn probabilities
    '''
    y_hat = model.predict_proba(account_data[FEATURES])
    y_hat_churn = y_hat[:,1]
    df = pd.DataFrame()
    df["account_id"] = account_data["account_id"]
    df["date"] = account_data["date"]
    df["prob_churn"] = y_hat_churn
    
    df.sort_values("prob_churn", ascending = False, inplace = True)
    return df
    
def save_likely_churns(prop : float, FILEPATH = ""):
    '''
    Saves a csv file containing a given proportion of the most likely accounts to churn
    Inputs:
        prop     : The top proportion of the accounts to be saved
        FILEPATH : The filepath to where the csv will be saved
    Outputs:
        None
    '''
    if (prop < 0 or prop > 1):
        raise Exception("Proportion must be between 0 and 1 inclusive")
        
    df = sort_by_prob()
    num = len(df.index)
    size = math.ceil(num * prop)
    
    df = df.iloc[:size]
    df.to_csv(f"{FILEPATH}Top_{size}_likely_churns.csv",index = False)
    
